import { createState } from "../mfkip/database";

export interface FileData {
    message: {
        name: string;
        size: number;
        type: string;
        image: string;
    };
    created: number;
}

export default createState<FileData>('file-db', []);