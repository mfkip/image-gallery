import App from './App.svelte';
import "@mfkip/web-lib";

const app = new App({
	target: document.body,
	props: {
		name: 'world'
	}
});

export default app;