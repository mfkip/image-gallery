import type EventStore from "orbit-db-eventstore";
import { fromEvent } from "rxjs";
import { derived, Readable, writable, Writable } from "svelte/store";
import mfkip from "./mfkip";

const startedIpfs = mfkip.start();

export async function createDb<T>(name: string) {
    await startedIpfs;
    const orbitdb = await mfkip.orbitdb;
    const store = await orbitdb.log<T>(name, { accessController: { write: ['*'] } })
    // wait for db to load
    await store.load();

    return store;
}

export async function createState<T>(name: string, initialValue: T[]): Promise<[state: Readable<typeof initialValue>, setState: (n: any) => Promise<void>]> {
    const state = writable<T[]>(initialValue);
    const eventStore = await createDb<T>(name);
    syncronize<T>(eventStore, state);

    const setState = async (value: T) => {
        const [_, err] = await publish<T>(eventStore, state, value);
        if(err) throw new Error(err);
    }

    return [derived(state, (n) => n), setState];
}

export function getEntryValues<T>(eventStore: EventStore<T>): T[] {
    // collect all entries from the event
    const entries = eventStore.iterator({ limit: -1 }).collect();
    // gather the values from each entry's payload
    return entries.map(entry => entry.payload.value);
}

export function syncronize<T>(eventStore: EventStore<T>, store: Writable<T[]>) {
    // subscribe to the replicate event from db
    // NOTE: replicate events wont fire for our own published events/chat
    fromEvent(eventStore.events, 'replicated')
        .subscribe(() => {
            // gather the values from each entry's payload
            store.set(getEntryValues(eventStore));
        });
    
    store.set(getEntryValues(eventStore));
}

export async function publish<T>(eventStore: EventStore<T>, store: Writable<T[]>, message: T) {
    let response: [success: string, err: string] = [null, null];
    try {
        response[0] = await eventStore.add({ message, created: Date.now() });
        store.set(getEntryValues(eventStore));
    } catch (e) {
        console.log(e);
        
        response[1] = e;
    }

    return response;
}