import  '@mfkip/web-lib';

const key = 'L2tleS9zd2FybS9wc2svMS4wLjAvCi9iYXNlMTYvCjdlMWQyYWY3MTU3MWYzYjRlNDEwZWQwMmFhOTk3NDc2MzkyNjZhNDVhZDgwNzYzNDUwZjk0YzM3YzUwYjY4NzE';

const partitions = prompt(
	"Enter partitions separated by commas (cancel for default):",
	"default"
);

localStorage._mfkip_partitions = [].concat(
	partitions.replace(" ", "").split(",")
);

const mfkip = (window as any).mfkip = new MFKIP({
	appId: 'anythingbro',
    partitions: localStorage._mfkip_partitions?.replace(' ', '').split(',') || ['default'],
    minConnections: 5,
	encodedKey: key,
	listenAddresses: ['/dns4/guarded-anchorage-63004.herokuapp.com//tcp/443/wss/p2p-webrtc-star'],
});

export default mfkip;